﻿using CarRentalWebService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CarRentalWebService.Controllers
{
    [Authorize]
    [RoutePrefix("api/Cars")]
    public class CarsController : ApiController
    {
        // GET api/cars
        public IEnumerable<CarModel> Get()
        {
            List<CarModel> cars = new List<CarModel>();

            cars = DBConnection.Instance.GetAllCars();

            return cars;
        }

        [Route("Rentals")]
        public IEnumerable<RentCarModel> GetRentals()
        {
            string username = User.Identity.Name;
            List<RentCarModel> rentals = new List<RentCarModel>();
            rentals = DBConnection.Instance.GetRentalsOfUser(username);
            return rentals;
        }

        [Route("Rent")]
        public void RentCar([FromBody] RentCarModel rental)
        {
            string username = User.Identity.Name;
            DBConnection.Instance.RentCar(rental.car.id, username, rental.rentStart, rental.rentEnd);
        }

        [Route("Return")]
        public void ReturnCar(int id)
        {
            string username = User.Identity.Name;
            DBConnection.Instance.ReturnCar(id, username);
        }
    }
}
