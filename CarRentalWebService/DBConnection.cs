﻿using CarRentalWebService.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CarRentalWebService
{
    public class DBConnection
    {
        private static DBConnection instance = null;
        private static readonly object padlock = new object();

        private string connectionString;

        public static DBConnection Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new DBConnection();
                    }
                    return instance;
                }
            }
        }

        protected DBConnection()
        {
            connectionString = @"Data Source=KALLUK\SQLEXPRESS;Initial Catalog=CarRental;Integrated Security=True";
            
        }

        public List<CarModel> GetAllCars()
        {
            List<CarModel> cars = new List<CarModel>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                // 1.  create a command object identifying the stored procedure
                SqlCommand cmd = new SqlCommand("GetFreeCars", connection);
                // 2. set the command object so it knows to execute a stored procedure
                cmd.CommandType = CommandType.StoredProcedure;

                // execute the command
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    
                    // iterate through results, printing each to console
                    while (rdr.Read())
                    {
                        int id = (int)rdr["Id"];
                        string model = (string)rdr["Model"];
                        double price = Double.Parse(rdr["Price"].ToString());
                        cars.Add(new CarModel
                        {
                            id = id,
                            model = model,
                            price = price
                        });
                    }
                }

                connection.Close();
            }
            return cars;
        }

        public List<RentCarModel> GetRentalsOfUser(string username)
        {
            List<RentCarModel> cars = new List<RentCarModel>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand cmd = new SqlCommand("GetRentalsOfUser", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@name", username));

                using (SqlDataReader rdr = cmd.ExecuteReader())
                {

                    while (rdr.Read())
                    {
                        int id = (int)rdr["Id"];
                        string model = (string)rdr["Model"];
                        double price = Double.Parse(rdr["Price"].ToString());
                        var car = new CarModel
                        {
                            id = id,
                            model = model,
                            price = price
                        };
                        var rentStart = (DateTime)rdr["RentStart"];
                        var rentEnd = (DateTime)rdr["RentEnd"];
                        cars.Add(new RentCarModel
                        {
                            car = car,
                            rentStart = rentStart,
                            rentEnd = rentEnd
                        });
                    }
                }

                connection.Close();
            }
            return cars;
        }

        public bool CreateUser(string username)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand cmd = new SqlCommand("CreateUser", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@name", username));

                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        
                    }
                }
                connection.Close();
            }
            return true;
        }

        public bool RentCar(int carId, string username, DateTime rentStart, DateTime rentEnd)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand cmd = new SqlCommand("RentCar", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@autoId", carId));
                cmd.Parameters.Add(new SqlParameter("@kunde", username));
                cmd.Parameters.Add(new SqlParameter("@rentStart", rentStart));
                cmd.Parameters.Add(new SqlParameter("@rentEnd", rentEnd));

                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                    }
                }

                connection.Close();
            }
            return true;
        }

        public bool ReturnCar(int carId, string username)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand cmd = new SqlCommand("ReturnCar", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@auto", carId));
                cmd.Parameters.Add(new SqlParameter("@name", username));

                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {

                    }
                }
                connection.Close();
            }
            return true;
        }
    }
}