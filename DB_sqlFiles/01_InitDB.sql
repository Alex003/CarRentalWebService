USE [master]
GO
/****** Object:  Database [CarRental]    Script Date: 23.04.2017 11:33:21 ******/
CREATE DATABASE [CarRental]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CarRental', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.SQLEXPRESS\MSSQL\DATA\CarRental.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'CarRental_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.SQLEXPRESS\MSSQL\DATA\CarRental_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [CarRental] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CarRental].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CarRental] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CarRental] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CarRental] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CarRental] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CarRental] SET ARITHABORT OFF 
GO
ALTER DATABASE [CarRental] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CarRental] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CarRental] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CarRental] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CarRental] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CarRental] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CarRental] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CarRental] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CarRental] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CarRental] SET  DISABLE_BROKER 
GO
ALTER DATABASE [CarRental] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CarRental] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CarRental] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CarRental] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CarRental] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CarRental] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CarRental] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CarRental] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [CarRental] SET  MULTI_USER 
GO
ALTER DATABASE [CarRental] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CarRental] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CarRental] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CarRental] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [CarRental] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [CarRental] SET QUERY_STORE = OFF
GO
USE [CarRental]
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [CarRental]
GO
/****** Object:  Table [dbo].[Autos]    Script Date: 23.04.2017 11:33:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Autos](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Model] [varchar](50) NOT NULL,
	[Price] [money] NOT NULL,
 CONSTRAINT [PK_Autos] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Kunden]    Script Date: 23.04.2017 11:33:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Kunden](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Kunden] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Verleihungen]    Script Date: 23.04.2017 11:33:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Verleihungen](
	[Auto] [int] NOT NULL,
	[Kunde] [int] NOT NULL,
	[RentStart] [datetime] NOT NULL,
	[RentEnd] [datetime] NOT NULL
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Autos] ON 

INSERT [dbo].[Autos] ([Id], [Model], [Price]) VALUES (1, N'VW Golf', 2000.5000)
INSERT [dbo].[Autos] ([Id], [Model], [Price]) VALUES (2, N'BMW asdf', 5000.0000)
INSERT [dbo].[Autos] ([Id], [Model], [Price]) VALUES (3, N'Audi A2', 8000.0000)
SET IDENTITY_INSERT [dbo].[Autos] OFF
SET IDENTITY_INSERT [dbo].[Kunden] ON 

INSERT [dbo].[Kunden] ([Id], [Name]) VALUES (1, N'Alex@asdf.at')
INSERT [dbo].[Kunden] ([Id], [Name]) VALUES (2, N'Felix@asdf.at')
INSERT [dbo].[Kunden] ([Id], [Name]) VALUES (3, N'Max@adsf.at')
SET IDENTITY_INSERT [dbo].[Kunden] OFF
/****** Object:  Index [IX_Verleihungen]    Script Date: 23.04.2017 11:33:22 ******/
ALTER TABLE [dbo].[Verleihungen] ADD  CONSTRAINT [IX_Verleihungen] UNIQUE NONCLUSTERED 
(
	[Auto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Verleihungen]  WITH CHECK ADD  CONSTRAINT [FK_Verleihungen_Autos] FOREIGN KEY([Auto])
REFERENCES [dbo].[Autos] ([Id])
GO
ALTER TABLE [dbo].[Verleihungen] CHECK CONSTRAINT [FK_Verleihungen_Autos]
GO
ALTER TABLE [dbo].[Verleihungen]  WITH CHECK ADD  CONSTRAINT [FK_Verleihungen_Kunden] FOREIGN KEY([Kunde])
REFERENCES [dbo].[Kunden] ([Id])
GO
ALTER TABLE [dbo].[Verleihungen] CHECK CONSTRAINT [FK_Verleihungen_Kunden]
GO
/****** Object:  StoredProcedure [dbo].[CreateUser]    Script Date: 23.04.2017 11:33:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alex
-- Create date: 23.4.17
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[CreateUser] 
	-- Add the parameters for the stored procedure here
	@name varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	insert into Kunden (Name) values (@name);
END

GO
/****** Object:  StoredProcedure [dbo].[GetAllCars]    Script Date: 23.04.2017 11:33:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alex
-- Create date: 22.4.17
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetAllCars] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from dbo.Autos; 
END

GO
/****** Object:  StoredProcedure [dbo].[GetFreeCars]    Script Date: 23.04.2017 11:33:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alex
-- Create date: 23.4.17
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetFreeCars] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT id, model, price 
	from autos left join Verleihungen
	on id = [auto]
	where [auto] is null;
END

GO
/****** Object:  StoredProcedure [dbo].[GetRentalsOfUser]    Script Date: 23.04.2017 11:33:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alex
-- Create date: 23.4.17
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetRentalsOfUser] 
	-- Add the parameters for the stored procedure here
	@name varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT autos.id, model, price, RentStart, RentEnd
	from Autos, Verleihungen, kunden
	where	autos.Id = verleihungen.[Auto] and
			Verleihungen.kunde = kunden.id and
			kunden.name = @name;
END

GO
/****** Object:  StoredProcedure [dbo].[RentCar]    Script Date: 23.04.2017 11:33:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alex
-- Create date: 22.4.17
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[RentCar] 
	-- Add the parameters for the stored procedure here
	@autoId int, 
	@kunde varchar(50),
	@rentStart datetime,
	@rentEnd datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF ((select count(*) from verleihungen where [auto] = @autoId) = 0)
		Insert into verleihungen ([auto], kunde, rentStart, rentEnd) values
			(@autoId, (select id from kunden where name = @kunde), @rentStart, @rentEnd);
END

GO
/****** Object:  StoredProcedure [dbo].[ReturnCar]    Script Date: 23.04.2017 11:33:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alex
-- Create date: 23.4.17
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[ReturnCar] 
	-- Add the parameters for the stored procedure here
	@auto int, 
	@name varchar(50) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	if (
	(select name from verleihungen, kunden 
	where kunde = kunden.id and [auto] = @auto) = @name)
	
	delete from verleihungen where [auto] = @auto;
END

GO
USE [master]
GO
ALTER DATABASE [CarRental] SET  READ_WRITE 
GO
