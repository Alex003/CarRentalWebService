USE [CarRental]
GO

DECLARE @RC int
DECLARE @autoId int
DECLARE @kunde varchar(50)
DECLARE @rentStart datetime
DECLARE @rentEnd datetime

-- TODO: Set parameter values here.
set @autoId = 1;
set @kunde = 'Alex@asdf.at';
set @rentStart = GETDATE();
set @rentEnd = GETDATE();
set @rentEnd = DateAdd(day, 7, @rentEnd);

EXECUTE @RC = [dbo].[RentCar] 
   @autoId
  ,@kunde
  ,@rentStart
  ,@rentEnd
GO


