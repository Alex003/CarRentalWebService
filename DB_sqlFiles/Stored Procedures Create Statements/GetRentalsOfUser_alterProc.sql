USE [CarRental]
GO
/****** Object:  StoredProcedure [dbo].[GetRentalsOfUser]    Script Date: 23.04.2017 09:48:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alex
-- Create date: 23.4.17
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[GetRentalsOfUser] 
	-- Add the parameters for the stored procedure here
	@name varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT autos.id, model, price, RentStart, RentEnd
	from Autos, Verleihungen, kunden
	where	autos.Id = verleihungen.[Auto] and
			Verleihungen.kunde = kunden.id and
			kunden.name = @name;
END
