-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alex
-- Create date: 23.4.17
-- Description:	
-- =============================================
CREATE PROCEDURE ReturnCar 
	-- Add the parameters for the stored procedure here
	@auto int, 
	@name varchar(50) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	if (
	(select name from verleihungen, kunden 
	where kunde = kunden.id and [auto] = @auto) = @name)
	
	delete from verleihungen where [auto] = @auto;
END
GO
