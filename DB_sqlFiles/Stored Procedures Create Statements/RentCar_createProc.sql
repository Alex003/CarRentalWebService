USE [CarRental]
GO
/****** Object:  StoredProcedure [dbo].[RentCar]    Script Date: 23.04.2017 09:33:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alex
-- Create date: 22.4.17
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[RentCar] 
	-- Add the parameters for the stored procedure here
	@autoId int, 
	@kunde varchar(50),
	@rentStart datetime,
	@rentEnd datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF ((select count(*) from verleihungen where [auto] = @autoId) = 0)
		Insert into verleihungen ([auto], kunde, rentStart, rentEnd) values
			(@autoId, (select id from kunden where name = @kunde), @rentStart, @rentEnd);
END
